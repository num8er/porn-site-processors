var config = require('./config.json');
var paths = config.paths;
var dbConfig = config.db;
var path = require('path');
var fs = require('fs.extra');
var exec = require('child_process').exec;
var MongoClient = require('mongodb');
var db, artistsCollection, categoriesCollection, videosCollection;
var request = require('request');
var cookieJar = request.jar();
var async = require('async');
var cheerio = require('cheerio');
var _ = require('underscore');
var md5 = require('md5');
var S = require('string');
var slugify = require('slugify');
var glob = require('glob');

function run() {
    console.log('START');

    MongoClient.connect(dbConfig.url, function(err, db) {
        if(err) {
            console.error(err);
            process.exit(0);
            return;
        }

        console.log('CONNECTED TO DB');
        artistsCollection = db.collection(dbConfig.artistsCollection);
        categoriesCollection = db.collection(dbConfig.categoriesCollection);
        videosCollection = db.collection(dbConfig.videosCollection);

        videosCollection.find({active: true}).toArray(function(err, result) {
            async.each(result,
                function(video, next) {
                    fixVideoData(video, next);
                },
                function(err, callback) {
                    process.exit(0);
                });
        });
    });

}
run();

function fixVideoData(video, callback) {
    var hash = video.hash;
    var duration = video.duration;
    var seconds = duration.split(':');
    seconds = parseInt(seconds[0])*60+parseInt(seconds[1]);

    var thumbs = video.thumbs;
    var thumbs_local = _.map(thumbs, function(thumb) {
        return '/'+paths.storage.thumbs + hash.substr(0, 4) + '/' + hash + '/' + md5(thumb)+path.extname(thumb);
    });

    var video_dir = __dirname + '/../'+paths.storage.videos + hash.substr(0, 4) + '/' + hash + '/*-360p.mp4';
    glob(video_dir, {}, function (err, files) {
        files = (files.length > 0)? {'360p': files[0].substr(10)} : {};

        updateVideoInDB(hash, {
            seconds: seconds,
            thumbs_local: thumbs_local,
            views: 0,
            files: files
        }, callback);
    });
}

function updateVideoInDB(hash, data, callback) {
    data.updatedAt = new Date();
    if(data.title) {
        data.slug = slugify(data.title);
    }

    videosCollection.updateOne({hash: hash}, {$set: data}, function(err, result) {
        if(err) {
            console.error(err);
            return;
        }

        callback();
    });
}