var config = require('./config.json');
var paths = config.paths;
var dbConfig = config.db;
var path = require('path');
var fs = require('fs.extra');
var exec = require('child_process').exec;
var MongoClient = require('mongodb');
var db, artistsCollection, categoriesCollection, videosCollection;
var request = require('request');
var cookieJar = request.jar();
var async = require('async');
var cheerio = require('cheerio');
var _ = require('underscore');
var md5 = require('md5');
var S = require('string');
var slugify = require('slugify');
var glob = require('glob');


MongoClient.connect(dbConfig.url, function(err, db) {
    if(err) {
        console.error(err);
        process.exit(0);
        return;
    }

    console.log('CONNECTED TO DB');
    artistsCollection = db.collection(dbConfig.artistsCollection);
    categoriesCollection = db.collection(dbConfig.categoriesCollection);
    videosCollection = db.collection(dbConfig.videosCollection);

    videosCollection.find({slug: {$exists: false}, active: true}).toArray(function(err, result) {
        async.each(result,
            function(record, next) {
                var filesToDelete = [];
                var thumbs = record.thumbs_local;
                thumbs.forEach(function(thumb) {
                    filesToDelete.push(thumb);
                });

                var files = record.files;
                for(var i in files) {
                    filesToDelete.push(files[i]);
                }

                async.each(filesToDelete,
                    function(file, next) {
                        file = __dirname + '/../' + file;
                        var dir = path.dirname(file);

                        fs.remove(dir, function(err) {
                            if(err) {
                                console.log(err);
                                next(err);
                                return;
                            }
                            next();
                        });
                    },
                    function(err) {
                        if(err) {
                            console.log(err);
                            next(err);
                            return;
                        }

                        videosCollection.deleteOne({
                            _id: record._id
                        }, function(err, result) {
                            next(err);
                        })
                    });
            },
            function(err, result) {
                console.log(err);
                process.exit(0);
            });
    });
});