var config = require('./config.json');
var paths = config.paths;
var dbConfig = config.db;
var path = require('path');
var fs = require('fs.extra');
var exec = require('child_process').exec;
var MongoClient = require('mongodb');
var db, artistsCollection, categoriesCollection, videosCollection;
var request = require('request');
var cookieJar = request.jar();
var async = require('async');
var cheerio = require('cheerio');
var _ = require('underscore');
var md5 = require('md5');
var S = require('string');
var slugify = require('slugify');
var glob = require('glob');

var URL_INITIAL = "http://www.porntube.com";
var URL_BASE = URL_INITIAL;
var URL_PREFIX_POPULAR = "http://www.porntube.com/videos?duration=medium&p=";
var URL_PREFIX_LATEST = "http://www.porntube.com/videos?sort=date&duration=medium&p=";
var URLS = [
    'http://www.porntube.com/tags/big-tits?duration=medium&p=',
    'http://www.porntube.com/tags/babe?duration=medium&p=',
    'http://www.porntube.com/tags/asian?duration=medium&p=',
    'http://www.porntube.com/tags/amateur?duration=medium&p=',
    'http://www.porntube.com/tags/big-dick?duration=medium&p=',
    'http://www.porntube.com/tags/cumshot?duration=medium&p=',
    'http://www.porntube.com/tags/double-penetration?duration=medium&p=',
    'http://www.porntube.com/tags/ebony?duration=medium&p=',
    'http://www.porntube.com/tags/ex-girlfriend?duration=medium&p=',
    'http://www.porntube.com/tags/fetish?duration=medium&p=',
    'http://www.porntube.com/tags/groupsex?duration=medium&p=',
    'http://www.porntube.com/tags/hardcore?duration=medium&p=',
    'http://www.porntube.com/tags/latina?duration=medium&p=',
    'http://www.porntube.com/tags/massage?duration=medium&p=',
    'http://www.porntube.com/tags/masturbation?duration=medium&p=',
    'http://www.porntube.com/tags/mature?duration=medium&p=',
    'http://www.porntube.com/tags/milf?duration=medium&p=',
    'http://www.porntube.com/tags/public?duration=medium&p=',
    'http://www.porntube.com/tags/teen?duration=medium&p=',
    'http://www.porntube.com/tags/solo?duration=medium&p=',
    'http://www.porntube.com/tags/anal?duration=medium&p='
];

function run() {
    console.log('START');
    var callStack = [];

    callStack.push(function(next){
        MongoClient.connect(dbConfig.url, function(err, db) {
            if(err) {
                console.error(err);
                process.exit(0);
                return;
            }

            console.log('CONNECTED TO DB');
            artistsCollection = db.collection(dbConfig.artistsCollection);
            categoriesCollection = db.collection(dbConfig.categoriesCollection);
            videosCollection = db.collection(dbConfig.videosCollection);

            videosCollection.find({active: {$exists: false}}).toArray(function(err, result) {
                saveVideos(result, function() {
                    next();
                });
            });
        });
    });

    callStack.push(function(next){
        getInitialPage(function(response, body) {
            if(response.statusCode == 200) {
                async.eachSeries(_.range(1, 5),
                    function(n, callback) {
                        async.eachSeries(URLS, function(URL_PREFIX, next) {
                            getPage(n, URL_PREFIX, function(){
                                next();
                            });
                        }, callback);
                    },
                    function(err, results) {
                        if(err) {
                            console.error(err);
                            return;
                        }

                        console.log('DONE');
                        next();
                    });
            }
        });
    });

    callStack.push(function(next){
        getInitialPage(function(response, body) {
            if(response.statusCode == 200) {
                console.log('PARSING LATEST PAGES');

                async.eachSeries(_.range(1, 5),
                    function(n, callback) {
                        getLatestPage(n, callback);
                    },
                    function(err, results) {
                        if(err) {
                            console.error(err);
                            return;
                        }

                        console.log('DONE');
                        next();
                    });
            }
        });
    });

    callStack.push(function(next){
        getInitialPage(function(response, body) {
            if(response.statusCode == 200) {
                console.log('PARSING POPULAR PAGES');

                async.eachSeries(_.range(1, 5),
                    function(n, callback) {
                        getPopularPage(n, callback);
                    },
                    function(err, results) {
                        if(err) {
                            console.error(err);
                            return;
                        }

                        console.log('DONE');
                        next();
                    });
            }
        });
    });

    callStack.push(function(next){
        videosCollection.find({active: {$exists: false}}).toArray(function(err, result) {
            saveVideos(result, function(){
                console.log('FINISHED');
                process.exit(0);
            });
        });
    });

    async.series(callStack);
}
run();

function useragent() {
    return config.useragents[Math.floor(Math.random()*config.useragents.length)];
}

var Referer;
function headers(referer) {
    var headers = {'User-Agent': useragent()};
    (referer)?
        Referer = referer
        : referer = Referer;
    headers.Referer = referer;
    headers.Origin = 'http://www.porntube.com';
    return headers;
}

function getInitialPage(callback) {
    console.log('GETTING INITIAL PAGE FOR COOKIES');

    doGet(URL_INITIAL, null, function(err, response, body) {
        if(response.statusCode == 200) {
            callback(response, body);
            return;
        }

        console.warn('PAGE CANNOT BE FETCHED');
    });
}

function getPage(n, prefix, callback) {
    console.log('GETTING PAGE: '+n);

    var referer = (n > 1)? prefix+(n-1) : null;
    var url = prefix + n;
    doGet(url, referer, function(err, response, body) {
        if(response.statusCode == 200) {
            console.log(response.statusCode);
            getVideos(body, callback);
            return;
        }

        console.error('PAGE CANNOT BE FETCHED');
        callback();
    });
}


function getPopularPage(n, callback) {
    console.log('GETTING PAGE: '+n);

    var referer = (n > 1)? URL_PREFIX_POPULAR+(n-1) : null;
    var url = URL_PREFIX_POPULAR + n;
    doGet(url, referer, function(err, response, body) {
        if(response.statusCode == 200) {
            console.log(response.statusCode);
            getVideos(body, callback);
            return;
        }

        console.error('PAGE CANNOT BE FETCHED');
        callback();
    });
}

function getLatestPage(n, callback) {
    console.log('GETTING PAGE: '+n);

    var referer = (n > 1)? URL_PREFIX_LATEST+(n-1) : null;
    var url = URL_PREFIX_LATEST + n;
    doGet(url, referer, function(err, response, body) {
        if(response.statusCode == 200) {
            console.log(response.statusCode);
            getVideos(body, callback);
            return;
        }

        console.error('PAGE CANNOT BE FETCHED');
        callback();
    });
}

function getVideos(html, callback) {
    var $ = cheerio.load(html);
    var videos = [];
    $('#video_list_column .thumb_video').each(function() {
        var link = $(this).find('.thumb-link').attr('href');
        var thumbs = [];
        $(this).find('ul.mini-slide>li').each(function() {thumbs.push($(this).attr('data-src'));});
        var duration = $(this).find('.duration-top').text();
        if(!duration) duration = null;

        var video = {
            link: link,
            thumbs: thumbs,
            duration: duration
        };
        videos.push(video);
    });

    async.eachSeries(videos,
        function(video, next) {
            saveVideoLink(video, next);
        },
        function(err, results) {
            callback(err, results);
        });
}

function saveVideoLink(video, callback) {
    var link = video.link;
    var thumbs = video.thumbs;
    var duration = video.duration;
    var hash = md5(link);

    videosCollection.find({hash: hash}).toArray(function(err, result) {
        if(!(result.length > 0)) {
            insertVideoToDB({
                hash: hash,
                link: link,
                thumbs: thumbs,
                duration: duration
            }, callback);
            return;
        }

        //console.log('VIDEO EXISTS', link);
        callback();
    });
}

function saveVideos(videos, callback) {
    if(videos) {
        console.log('SAVING VIDEO CONTENTS');

        async.eachSeries(videos, saveVideo, callback);
        return;
    }

    callback();
}

function saveVideo(video, callback) {
    var hash = video.hash;
    var link = video.link;
    var thumbs = video.thumbs;

    console.log('');
    console.log('---');
    console.log('');

    async.series([
        function(callback) {
            downloadVideoInfo(link, hash, callback);
        },
        function(callback) {
            downloadThumbs(thumbs, hash, callback);
        },
        function(callback) {
            downloadVideo(link, hash, callback);
        }
    ], function(err, results) {
        if(err) {
            console.error(err);
            callback();
            return;
        }

        fixVideoData(video, function() {
            updateVideoInDB(hash, {active: true}, callback);
        });
    });
}

function fixVideoData(video, callback) {
    var hash = video.hash;
    var duration = video.duration;
    var seconds = duration.split(':');
    seconds = parseInt(seconds[0])*60+parseInt(seconds[1]);

    var thumbs = video.thumbs;
    var thumbs_local = _.map(thumbs, function(thumb) {
        return '/'+paths.storage.thumbs + hash.substr(0, 4) + '/' + hash + '/' + md5(thumb)+path.extname(thumb);
    });

    var video_dir = __dirname + '/../'+paths.storage.videos + hash.substr(0, 4) + '/' + hash + '/*-360p.mp4';
    glob(video_dir, {}, function (err, files) {
        files = (files.length > 0)? {'360p': files[0].substr(10)} : {};

        updateVideoInDB(hash, {
            seconds: seconds,
            thumbs_local: thumbs_local,
            views: 0,
            files: files
        }, callback);
    });
}

function returnArtistPath(artist) {
    var artist_path = './../'+paths.storage.artists+'/'+slugify(artist);
    var stats = null; try {stats = fs.statSync(artist_path);} catch (e) {stats = null}
    if(!(stats && stats.isDirectory())) {
        fs.mkdirpSync(artist_path);
    }

    return path.normalize(artist_path);
}

function makeThumbsPath(hash, callback) {
    var hash_part = hash.substr(0, 4);
    var thumbs_path = './../'+paths.storage.thumbs+'/'+hash_part+'/'+hash+'/';
    fs.mkdirp(thumbs_path, function (err) {
        thumbs_path = path.normalize(thumbs_path);
        callback(err, thumbs_path);
    });
}

function makeVideoPath(hash, callback) {
    var hash_part = hash.substr(0, 4);
    var video_path = './../'+paths.storage.videos+'/'+hash_part+'/'+hash+'/';
    fs.mkdirp(video_path, function (err) {
        video_path = path.normalize(video_path);
        callback(err, video_path);
    });
}

function downloadVideo(link, hash, callback) {
    console.log('TRYING TO DOWNLOAD VIDEO: ', link);
    doGet(link, null, function(err, response) {
        if(err) {
            callback(err);
            return;
        }

        if(response.statusCode == 200) {
            var $ = cheerio.load(response.body);
            var $buttons = $('#download-links').find('button');

            var linkId = null;
            $buttons.each(function(){
                if($(this).attr('data-quality') == '360') {
                    linkId = $(this).attr('data-id');
                }
            });

            var downloadLink = 'http://tkn.porntube.com/'+linkId+'/download/360';
            doPost(downloadLink, link, function(err, response, body) {
                if(err) {
                    callback(err);
                    return;
                }
                if(response.statusCode == 200) {
                    body = JSON.parse(body);
                    if(body.token) {
                        console.log('GOT DOWNLOAD TOKEN: '+body.token);
                        makeVideoPath(hash, function(err, video_path) {
                            if(err) {
                                callback(err);
                                return;
                            }

                            var filename = _.last(link.split('/'));
                            filename = _.first(filename.split('_'))+'-360p.mp4';
                            downloadVideoFile(body.token, video_path, filename, callback);
                        });

                        return;
                    }
                }

                callback('RESPONSE IS NOT AS EXPECTED: ', response.body, ' CODE: ', response.statusCode);
            });
            return;
        }

        callback('PAGE CANNOT BE FETCHED');
    });
}

function downloadVideoInfo(link, hash, callback) {
    console.log('GETTING INFO OF VIDEO: '+link);
    doGet(link, null, function(err, response) {
        if(err) {
            callback(err);
            return;
        }

        if(response.statusCode == 200) {
            var $ = cheerio.load(response.body);
            var title = $('#tab1').find('.title').text();
            if(!title || title == '') {
                var title = $('meta[itemprop="name"]').attr('content');
            }

            var artists = [];
            var $pornlist = $('.related-content .pornlist>li');
            $pornlist.each(function() {
                var $img = $(this).find('.porndetail.nothover .thumb-link>.thumb img');
                var artist = $img.attr('alt');
                if(artist) {
                    artist = S(artist).trim().s;
                    artists.push(artist);
                    artistsCollection.find({name: artist}).toArray(function(err, result) {
                        if(!(result.length > 0)) insertArtistToDB({name: artist}, function(){});
                    });
                    var src = $img.attr('src');
                    var file = returnArtistPath(artist) + '/' + slugify(artist) + '-' + _.last(src.split('/'));
                    exec("wget -nc -O " + file + " " + src, function () {});
                }
            });

            var categories = [];
            var $taglist = $('.related-content .tags>.list>li');
            $taglist.each(function() {
                var $a = $(this).find('a');
                var category = $a.text();
                if(category) {
                    category = S(category).trim().s;
                    categories.push(category);
                    categoriesCollection.find({name: category}).toArray(function(err, result) {
                        if(!(result.length > 0)) insertCategoryToDB({name: category}, function(){});
                    });
                }
            });

            console.log('Title: ' + title)
            console.log('Artists: ' + artists.join(', '));
            console.log('Categories: ' + categories.join(', '));

            updateVideoInDB(hash, {
                title: title,
                artists: artists,
                categories: categories
            }, callback);
            return;
        }

        callback('PAGE CANNOT BE FETCHED');
    });
}

function downloadThumbs(thumbs, hash, callback) {
    makeThumbsPath(hash, function(err, thumbs_path) {
        if(err) {
            callback(err);
            return;
        }

        console.log('DOWNLOADING THUMBS TO: ', thumbs_path);
        async.each(thumbs, function(thumb, callback) {
            downloadThumb(thumb, thumbs_path, callback);
        }, callback);
    });
}

function downloadThumb(thumb, thumbs_path, callback) {
    var thumb_file = thumbs_path+'/'+md5(thumb)+path.extname(thumb);
    var stats = null; try {stats = fs.statSync(thumb_file);} catch (e) {stats = null}
    if(stats && stats.isFile()) {
        callback();
        return;
    }

    exec("wget -nc -O " + thumb_file + " " + thumb, function (err, stdout, stderr) {
        if(err) {
            console.log('CANNOT DOWNLOAD: ', thumb, ' TO: ', thumb_file)
            console.log(err);
            callback(err);
            return;
        }

        callback();
    });
}

function downloadVideoFile(link, video_path, filename, callback) {
    var video_file = video_path+'/'+filename;
    var stats = null; try {stats = fs.statSync(video_file);} catch (e) {stats = null}
    if(stats && stats.isFile()) {
        callback();
        return;
    }

    var fileStream = fs.createWriteStream(video_file);
    fileStream
        .on('data', function(){
            console.log('data');
        })
        .on('error', function(err){callback(err);})
        .on('finish', function(){callback();});
    returnGetRequest(link).pipe(fileStream);
}

function updateVideoInDB(hash, data, callback) {
    data.updatedAt = new Date();
    if(data.title) {
        data.slug = slugify(data.title);
    }

    videosCollection.updateOne({hash: hash}, {$set: data}, function(err, result) {
        if(err) {
            console.error(err);
            return;
        }

        callback();
    });
}

function insertVideoToDB(data, callback) {
    data.createdAt = new Date();
    data.updatedAt = new Date();
    if(data.title) {
        data.slug = slugify(data.title);
    }

    videosCollection.insertMany([
        data
    ], function(err, result) {
        if(err) {
            console.error(err);
            return;
        }

        callback();
    });
}

function insertArtistToDB(data, callback) {
    data.createdAt = new Date();
    data.updatedAt = new Date();
    if(data.name) {
        data.slug = slugify(data.name);
    }

    artistsCollection.insertMany([
        data
    ], function(err, result) {
        if(err) {
            console.error(err);
            return;
        }

        callback();
    });
}

function insertCategoryToDB(data, callback) {
    data.createdAt = new Date();
    data.updatedAt = new Date();
    if(data.name) {
        data.slug = slugify(data.name);
    }

    categoriesCollection.insertMany([
        data
    ], function(err, result) {
        if(err) {
            console.error(err);
            return;
        }

        callback();
    });
}

function doGet(url, referer, callback) {
    console.log('GET '+url);

    if(url.indexOf(URL_BASE) != 0) {
        url = URL_BASE + url;
    }

    request.get({url: url, headers: headers(referer), jar: cookieJar}, function(err, response, body) {
        if (err) {
            console.error(err);
            callback(err);
            return;
        }

        callback(null, response, body);
    });
}

function doPost(url, referer, callback) {
    console.log('POST '+url);
    request.post({url: url, headers: headers(referer), jar: cookieJar}, function(err, response, body) {
        if (err) {
            console.error(err);
            callback(err);
            return;
        }

        callback(err, response, body);
    });
}

function returnGetRequest(url, referer) {
    return request({url: url, headers: headers(referer), jar: cookieJar});
}
